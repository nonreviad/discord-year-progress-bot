use serde::{Serialize, Deserialize};
use serenity::{client::Context, all::{GuildId, ChannelId}};
use sqlx::{Sqlite, Pool};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ServerData {
    pub guild_id: u64,
    pub channel_id: u64,
    pub timezone: String,
    pub hour: u32,
    pub minute: u32,
    pub last_notification: i64 // UNIX timestamp in ms
}

pub enum BotMessage {
    Init {
        ctx: Context,
    },
    Help {
        guild: GuildId,
        channel: ChannelId
    },
    Info {
        guild: GuildId,
        channel: ChannelId
    },
    Timezone {
        guild: GuildId,
        channel: ChannelId,
        timezone: String
    },
    NotificationTime {
        guild: GuildId,
        channel: ChannelId,
        hour: u32,
        minute: u32,
    },
    UpdateChannel {
        guild: GuildId,
        channel: ChannelId
    },
    Donezo
}