#![allow(dead_code)]
#![allow(unused_variables)]
#![allow(unused_labels)]
#![allow(unreachable_code)]
#![allow(unused_mut)]
#![allow(unused_assignments)]
use std::collections::HashMap;
use std::time::{SystemTime, Duration};

use anyhow::Result;
use chrono::{TimeZone, NaiveDate, NaiveDateTime, Datelike, Timelike, DateTime};
use serenity::all::{ChannelId, Channel};
use serenity::client::Context;
use sqlx::{Sqlite, Pool};
use tokio::sync::oneshot::Receiver as OneShotReceiver;
use tokio::sync::mpsc::Receiver as Receiver;
use chrono_tz::Tz;

use crate::messages::ServerData;
use crate::messages::BotMessage;

static HELP_MESSAGE: &'static str =
"Available commands:\n!help - prints out this message\n!info - Prints out what settings exist for your server right now.\n!update-channel - Updates the channel where progress reports are printed.\n!update-tz <tz> - Updates the timezone in which the notifications are done.\n!update-notif-time <HH:MM> - 24h format for when notifications should be sent out.";


pub async fn update_data(server_data: &ServerData, pool: &Pool<Sqlite>) -> Result<()> {
    let guild_id = server_data.guild_id as i64;
    let channel_id = server_data.channel_id as i64;
    let timezone = server_data.timezone.clone();
    let hour = server_data.hour as i64;
    let minute = server_data.minute as i64;
    sqlx::query!("UPDATE server_data SET channel_id=?, timezone=?, hour=?, minute=? WHERE guild_id=?", channel_id, timezone, hour, minute, guild_id)
    .execute(pool)
    .await?;
    sqlx::query!("INSERT INTO server_data (guild_id, channel_id, timezone, hour, minute) VALUES (?, ?, ?, ?, ?)", guild_id, channel_id, timezone, hour, minute)
    .execute(pool)
    .await?;
    Ok(())
}

pub async fn notifier_main_loop(mut rx: Receiver<BotMessage>, pool: Pool<Sqlite>) -> Result<()> {
    let mut last_update = 0u64;
    const UPDATE_POLL_TIME_IN_SECS: u64 = 3u64;
    let mut ctx: Option<Context> = None;
    let mut settings: HashMap<u64, ServerData> = HashMap::new();
    let data = sqlx::query!(r#"
    SELECT server_data.guild_id as guild_id, channel_id, timezone, hour, minute, max(last_update.post_time) as last_notification
    FROM server_data LEFT JOIN last_update ON server_data.guild_id = last_update.guild_id
    GROUP BY server_data.guild_id
    "#).fetch_all(&pool).await.unwrap();
    for record in data.into_iter() {
        let server_data = ServerData {
            guild_id: (record.guild_id.unwrap() as u64).into(),
            channel_id: (record.channel_id.unwrap() as u64).into(),
            timezone: record.timezone.unwrap().clone(),
            hour: record.hour.unwrap() as u32,
            minute: record.minute.unwrap() as u32,
            last_notification: record.last_notification.unwrap_or(0i64),
        };
        settings.insert(server_data.guild_id, server_data);
    }
    'loopy_mc_loopface: loop {
        let now = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH)?.as_secs();
        let now_as_millis = (now as i64) * 1000;
        #[allow(deprecated)]
        let now_as_datetime = NaiveDateTime::from_timestamp_millis(now_as_millis).unwrap();
        if (now - last_update) >= UPDATE_POLL_TIME_IN_SECS {
            // Do the thing
            if ctx.is_some() {
                for (guild_id, data) in settings.iter_mut() {
                    if data.channel_id != 0 {
                        if let Ok(tz) = Tz::from_str_insensitive(&data.timezone) {
                            #[allow(deprecated)]
                            if let Some(last_update) = NaiveDateTime::from_timestamp_millis(data.last_notification) {
                                let localised_date_time_for_last_update = tz.from_utc_datetime(&last_update);
                                let localised_current_time = tz.from_utc_datetime(&now_as_datetime);
                                if localised_current_time.day() != localised_date_time_for_last_update.day() || localised_current_time.month() != localised_date_time_for_last_update.month() || localised_current_time.year() != localised_date_time_for_last_update.year() {
                                    println!("Should update {}", data.guild_id);
                                    if (localised_current_time.hour() == data.hour && localised_current_time.minute() >= data.minute) || localised_current_time.hour() > data.hour {
                                        let start_of_year = tz.with_ymd_and_hms(localised_current_time.year(), 1, 1, 0, 0, 0);
                                        let start_of_next_year = tz.with_ymd_and_hms(localised_current_time.year() + 1, 1, 1, 0, 0, 0);
                                        match start_of_year {
                                            chrono::LocalResult::Single(date_time_for_start_of_year) => {
                                                let days_elapsed = 1 + localised_current_time.signed_duration_since(&date_time_for_start_of_year).num_days();
                                                match start_of_next_year {
                                                    chrono::LocalResult::Single(date_time_for_start_of_next_year) => {
                                                        let days_in_year = date_time_for_start_of_next_year.signed_duration_since(&date_time_for_start_of_year).num_days();
                                                        // This is not a misspelling, it's progress that's pog.
                                                        let pogress = (days_elapsed as f64) / (days_in_year as f64) * 100.0;
                                                        let channel: ChannelId = data.channel_id.into();
                                                        let _ = channel.say(&ctx.clone().unwrap().http, format!("📅 {} is {:.2}% done ⌛.", localised_current_time.year(), pogress).as_str()).await;
                                                        data.last_notification = now_as_millis;
                                                        let guild_id = guild_id.clone() as i64;
                                                        let _ = sqlx::query!("INSERT INTO last_update (guild_id, post_time) VALUES (?, ?)", guild_id, now_as_millis)
                                                        .execute(&pool)
                                                        .await;
                                                    }
                                                    _ => {
                                                        // Do nothing
                                                        println!("Failed to parse start of year")
                                                    }
                                                }
                                            },
                                           _ => {
                                            // Should not get here.
                                           }
                                        }
                                    }
                                }
                            } else {
                                println!("Cannot convert last update to local timezone");
                            }
                        } else {
                            println!("Invalid timezone {}", data.timezone);
                        }
                    }
                }
                last_update = now;
            } else {
                
            }
        }

        let sleepy_time = u64::max(UPDATE_POLL_TIME_IN_SECS, UPDATE_POLL_TIME_IN_SECS - now + last_update);

        tokio::select! {
            _ = tokio::time::sleep(Duration::from_secs(sleepy_time)) => {
                continue;
            }
            data = rx.recv() => {
                if let Some(data) = data {
                    match data {
                        BotMessage::Donezo => break 'loopy_mc_loopface,
                        BotMessage::Help{
                            guild, channel
                        } => {
                            if let Some(ref ctx) = ctx.clone() {
                                let _ = channel.say(&ctx.http, HELP_MESSAGE).await;
                            }
                        },
                        BotMessage::Info {
                            guild, channel
                        } => {
                            let data = settings.get(&guild.into());
                            if let Some(data) = data {
                                let mut response = String::new();
                                if data.channel_id == 0 {
                                    response.push_str("No channel :(\n");
                                } else {
                                    let response_channel: u64 = data.channel_id;
                                    response.push_str(format!("Channel: <#{}>\n", response_channel).as_str());
                                }
                                response.push_str(format!("Update time: {:02}:{:02} in {} timezone\n", data.hour, data.minute, data.timezone).as_str());
                                let _ = channel.say(&ctx.clone().unwrap().http, response).await;
                            } else {
                                let _ = channel.say(&ctx.clone().unwrap().http, "No info setup for this server yet :(").await;
                            }
                        },
                        BotMessage::Timezone {
                            guild, channel, timezone
                        } => {
                            if let Ok(tz) = timezone.parse::<Tz>() {
                                let data = settings.get_mut(&guild.into());
                                if let Some(data) = data {
                                    data.timezone = timezone;
                                    let _ = update_data(data, &pool).await;
                                    let _ = channel.say(&ctx.clone().unwrap().http, "Updated timezone ^_^").await;
                                } else {
                                    let data = ServerData { 
                                        guild_id: guild.into(), channel_id: 0, timezone, hour: 12, minute: 0, last_notification: 0 };
                                    let _ = update_data(&data, &pool).await;
                                    settings.insert(guild.into(), data);
                                    let _ = channel.say(&ctx.clone().unwrap().http, "Set timezone ^_^").await;
                                }
                            } else {
                                let _ = channel.say(&ctx.clone().unwrap().http, format!("Unknown timezone {timezone}")).await;
                            }
                        },
                        BotMessage::NotificationTime {
                            guild, channel, hour, minute
                        } => {
                            let data = settings.get_mut(&guild.into());
                            if let Some(data) = data {
                                data.hour = hour;
                                data.minute = minute;
                                let _ = update_data(data, &pool).await;
                                let _ = channel.say(&ctx.clone().unwrap().http, "Updated notification time ^_^").await;
                            } else {
                                let data = ServerData { 
                                    guild_id: guild.into(),
                                    channel_id: 0,
                                    timezone: "Etc/UTC".to_owned(),
                                    hour,
                                    minute,
                                    last_notification: 0
                                };
                                let _ = update_data(&data, &pool).await;
                                settings.insert(guild.into(), data);
                                let _ = channel.say(&ctx.clone().unwrap().http, "Set notification time ^_^").await;
                            }
                        },
                        BotMessage::UpdateChannel {
                            guild, channel
                        } => {
                            let data = settings.get_mut(&guild.into());
                            if let Some(data) = data {
                                data.channel_id = channel.into();
                                let _ = update_data(data, &pool).await;
                                let _ = channel.say(&ctx.clone().unwrap().http, "Updated to use this channel ^_^").await;
                            } else {
                                let data = ServerData{ guild_id: guild.into(), channel_id: channel.into(), timezone: "Etc/UTC".to_owned(), hour: 12, minute: 0, last_notification: 0 };
                                let _ = update_data(&data, &pool).await;
                                settings.insert(guild.into(), data);
                                let _ = channel.say(&ctx.clone().unwrap().http, "Set up to use this channel ^_^").await;
                            }
                        },
                        BotMessage::Init {
                            ctx: new_ctx,
                        } => {
                            ctx = Some(new_ctx);
                        }, 
                    }
                }
            }
        };
    }
    Ok(())
}