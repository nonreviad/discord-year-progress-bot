#![allow(dead_code)]
#![allow(unused_variables)]
#![allow(unused_labels)]
#![allow(unreachable_code)]
#![allow(unused_mut)]
#![allow(unused_imports)]
use std::borrow::BorrowMut;
use std::collections::HashMap;
use std::env;

use anyhow::Result;
use messages::BotMessage;
use notifier::notifier_main_loop;
use regex::Regex;
use serde::{Serialize, Deserialize};
use serenity::all::{Message, Role};
use serenity::client::Context;
use serenity::model::permissions;
use serenity::utils::Content;
use serenity::{Client, client::EventHandler};
use serenity::model::gateway::{GatewayIntents, Ready};
use sqlx::migrate::MigrateDatabase;
use sqlx::sqlite::SqlitePoolOptions;
use sqlx::{Sqlite, Pool};
use tokio::sync::oneshot::Sender as OneShotSender;
use tokio::sync::oneshot::Receiver as OneShotReceiver;
use tokio::sync::mpsc::{Receiver, Sender};
// TODO: I think this is now part of Rust proper, but maybe it can't use
// regular async traits for another reason?
use serenity::async_trait;
use tokio::sync::Mutex;

pub mod notifier;
pub mod messages;

struct Handler {
    tx: Sender<BotMessage>,
}

fn remove_mentions(s: &str) -> String {
    let mention_regex = Regex::new("<@[0-9]+>").unwrap();
    mention_regex.replace_all(s, "").to_string()
}

#[async_trait]
impl EventHandler for Handler {
    async fn ready(&self, ctx: Context, _: Ready) {
        let _ = self.tx.send(BotMessage::Init {
            ctx: ctx.clone(),
        }).await;
    }

    async fn message(&self, ctx: Context, message: Message) {
        if message.content.is_empty() {
            return;
        }
        if message.guild_id.is_none() {
            return;
        }
        let guild_id = message.guild_id.unwrap();
        let member = guild_id.member(&ctx.http, &message.author).await;
        if member.is_err() {
            return;
        }
        let member = member.unwrap();
        println!("Got message {} from {}",message.content, member.user.name);
        if member.user.name == "elgranguiri" || member.user.name == "andreionea" {
            let _ = message.reply(&ctx.http, "Fuck you 🖕 ❤️").await;
            return;
        }
        #[allow(deprecated)]
        let perms = member.permissions(&ctx.cache);
        if perms.is_err() {
            return;
        }
        let perms = perms.unwrap();
        if !perms.administrator() {
            return;
        }
        // Commands: !help, !update-channel, !update-tz, !update-notif-time
        let content = remove_mentions(&message.content).trim().to_owned();
        if content.starts_with("!help") {
            let _ = self.tx.send(BotMessage::Help {
                guild: message.guild_id.unwrap(),
                channel: message.channel_id
            }).await;
        } else if content.starts_with("!info"){
            let _ = self.tx.send(BotMessage::Info {
                guild: message.guild_id.unwrap(),
                channel: message.channel_id
            }).await;
        } else if content.starts_with("!update-channel") {
            let _ = self.tx.send(BotMessage::UpdateChannel {
                guild: message.guild_id.unwrap(),
                channel: message.channel_id
            }).await;
        } else if content.starts_with("!update-tz ") {
            let splits: Vec<&str> = content.split(" ").collect();
            if splits.len() < 2 {
                let _ = message.channel_id.say(&ctx.http, "No timezone specified").await;
            } else {
                let timezone = splits[1];
                let _ = self.tx.send(BotMessage::Timezone { guild: message.guild_id.unwrap(), channel: message.channel_id, timezone: timezone.to_owned() }).await;
            }
        } else if content.starts_with("!update-notif-time ") {
            let splits: Vec<&str> = content.split(" ").collect();
            if splits.len() < 2 {
                let _ = message.channel_id.say(&ctx.http, "No time specified").await;
            } else {
                let time = splits[1];
                let hour_minute = time.split_once(":");
                if let Some(hour_minute) = hour_minute {
                    let (hour, minute) = hour_minute;
                    if let Ok(hour) = hour.parse::<u32>() {
                        if let Ok(minute) = minute.parse::<u32>() {
                            if hour > 24 || minute > 60 {
                                let _ = message.channel_id.say(&ctx.http, "Invalid time format. Must be HH:MM format in 24h clock.").await;
                            } else {
                                let _ = self.tx.send(BotMessage::NotificationTime { guild: message.guild_id.unwrap(), channel: message.channel_id, hour, minute }).await;
                            }
                        } else {
                            let _ = message.channel_id.say(&ctx.http, "Invalid time format. Must be HH:MM format in 24h clock.").await;
                        }
                    } else {
                        let _ = message.channel_id.say(&ctx.http, "Invalid time format. Must be HH:MM format in 24h clock.").await;    
                    }
                } else {
                    let _ = message.channel_id.say(&ctx.http, "Invalid time format. Must be HH:MM format in 24h clock.").await;
                }
            }
        }
        else {
            println!("Unknown command: {content}");
        }
    }
}

#[tokio::main]
async fn main() -> Result<()>{
    dotenv::dotenv().ok();
    let token = env::var("DISCORD_TOKEN").expect("Expected a token in the environment");
    let db_url = env::var("DISCORD_DB_URL").expect("Expected a db url in the environment");
    println!("DB url = {db_url}");
    if !sqlx::Sqlite::database_exists(&db_url).await? {
        println!("Creating db");
        sqlx::Sqlite::create_database(&db_url).await?;
        println!("Created db");
    }
    let db = sqlx::SqlitePool::connect(&db_url).await?;
    println!("Running migrations");
    sqlx::migrate!("./migrations").run(&db).await?;
    println!("Finished migrations");
    let (tx, rx) = tokio::sync::mpsc::channel::<BotMessage>(1024);
    let mut client =
        Client::builder(&token, GatewayIntents::GUILDS | GatewayIntents::GUILD_MESSAGES | GatewayIntents::DIRECT_MESSAGES)
        .event_handler(Handler {
            tx,
        })
        .await
        .expect("Err creating client");
    let _ = tokio::join!(client.start(), notifier_main_loop(rx, db));
    Ok(())
}

#[cfg(test)]

mod test {
    use super::*;

    #[test]
    pub fn test_remove_single_mention() {
        let removed_single_mention = remove_mentions("<@1195429719945003089> sldfkjasldkfjalsdkfj;lkj");
        assert_eq!(removed_single_mention, " sldfkjasldkfjalsdkfj;lkj");
    }
    #[test]
    pub fn test_remove_multiple_mention() {
        let removed_single_mention = remove_mentions("<@1195429719945003089> sldfkjasldkfjalsdkfj;lkj <@1195429719945013089> sdafasdf");
        assert_eq!(removed_single_mention, " sldfkjasldkfjalsdkfj;lkj  sdafasdf");
    }
}