# syntax=docker/dockerfile:1

ARG RUST_VERSION=1.82.0
ARG APP_NAME=discord-year-progress-bot
FROM --platform=$BUILDPLATFORM tonistiigi/xx:1.3.0 AS xx
FROM --platform=$BUILDPLATFORM rust:${RUST_VERSION}-alpine AS build
ARG APP_NAME
WORKDIR /app

COPY --from=xx / /
RUN apk add --no-cache clang lld musl-dev git file
ARG TARGETPLATFORM
RUN xx-apk add --no-cache musl-dev gcc
ENV DATABASE_URL=sqlite:initial_table.sql
RUN --mount=type=bind,source=src,target=src \
    --mount=type=bind,source=Cargo.toml,target=Cargo.toml \
    --mount=type=bind,source=Cargo.lock,target=Cargo.lock \
    --mount=type=bind,source=migrations,target=migrations \
    --mount=type=bind,source=initial_table.sql,target=initial_table.sql \
    --mount=type=cache,target=/app/target/,id=rust-cache-${APP_NAME}-${TARGETPLATFORM} \
    --mount=type=cache,target=/usr/local/cargo/git/db \
    --mount=type=cache,target=/usr/local/cargo/registry/ \
    <<EOF
set -e
xx-cargo build --locked --release --target-dir ./target
cp ./target/$(xx-cargo --print-target-triple)/release/$APP_NAME /bin/server
xx-verify /bin/server
EOF

FROM alpine:3.18 AS final

COPY --from=build /bin/server /bin/

CMD ["/bin/server"]
