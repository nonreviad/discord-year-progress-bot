CREATE TABLE IF NOT EXISTS server_data (
    guild_id INTEGER PRIMARY KEY,
    channel_id INTEGER DEFAULT 0,
    timezone TEXT NOT NULL,
    hour INTEGER DEFAULT 12,
    minute INTEGER DEFAULT 0
);

CREATE TABLE IF NOT EXISTS last_update (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    post_time INTEGER DEFAULT 0,
    guild_id INTEGER DEFAULT 0,
    FOREIGN KEY (guild_id)
       REFERENCES server_data (guild_id) 
);


