#!/bin/bash
# This should be inferred from Cargo.toml
VERSION=0.4.1
docker buildx build --platform linux/amd64,linux/arm64 -t dypb-${VERSION} .
docker image tag dypb-${VERSION} nonreviad/discord-year-progress-bot:${VERSION}
docker image tag dypb-${VERSION} nonreviad/discord-year-progress-bot:latest
docker image push nonreviad/discord-year-progress-bot:${VERSION}
docker image push nonreviad/discord-year-progress-bot:latest